package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import java.lang.reflect.Array;

public class VueGenerale extends Fragment {

    // TODO Q1
    String salle;
    String poste;
    // TODO Q2.c
    SuiviViewModel model;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO Q1
        final String DISTANCIEL = getContext().getResources().getStringArray(R.array.list_salles)[0];
        salle = DISTANCIEL;
        poste = "";
        // TODO Q2.c
        MainActivity activity = (MainActivity) getActivity();
        model = activity.getModel();
        // TODO Q4

        Spinner spSalle = view.findViewById((R.id.spSalle));

        spSalle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                salle = spSalle.getSelectedItem().toString();
                update(view);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        ArrayAdapter<CharSequence> adapterSalle = ArrayAdapter.createFromResource(getActivity().getApplicationContext(), R.array.list_salles, android.R.layout.simple_spinner_item);
        adapterSalle.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSalle.setAdapter(adapterSalle);

        Spinner spPoste = view.findViewById((R.id.spPoste));

        spPoste.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                poste = spPoste.getSelectedItem().toString();
                update(view);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        ArrayAdapter<CharSequence> adapterPoste = ArrayAdapter.createFromResource(getActivity().getApplicationContext(), R.array.list_postes, android.R.layout.simple_spinner_item);
        adapterPoste.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPoste.setAdapter(adapterPoste);

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            TextView tvLogin = view.findViewById(R.id.tvLogin);
            model.setUsername(tvLogin.getText().toString());
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        // TODO Q5.b
        update(view);
        // TODO Q9
    }

    // TODO Q5.a
    public void update(View view) {
        Spinner spPoste = getView().findViewById(R.id.spPoste);
        if(salle.equals("Distanciel")) {
            spPoste.setVisibility(View.INVISIBLE);
            spPoste.setEnabled(false);
            this.model.setLocalisation("Distanciel");
        }
        else {
            spPoste.setVisibility(View.VISIBLE);
            spPoste.setEnabled(true);
            this.model.setLocalisation(salle + " : " + poste);
        }
    }
    // TODO Q9
}