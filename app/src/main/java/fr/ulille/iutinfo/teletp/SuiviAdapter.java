package fr.ulille.iutinfo.teletp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SuiviAdapter /* TODO Q6.a */extends RecyclerView.Adapter<SuiviAdapter.ViewHolder> {
    // TODO Q6.a
    private SuiviViewModel model;
    private LayoutInflater inflater;

    public SuiviAdapter(@NonNull Context context, int resource, SuiviViewModel model) {
        this.model = model;
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.question_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String question = model.getQuestion(position);
        holder.myTextView.setText(question);
        // TODO Q7
        if(model.getNextQuestion() > position) {
            holder.checkBox.setEnabled(true);
        }
    }

    @Override
    public int getItemCount() {
        return model.questions.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView myTextView;
        CheckBox checkBox;
        ViewHolder(View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.question);
            checkBox = itemView.findViewById(R.id.checkBox);
        }
    }


}

